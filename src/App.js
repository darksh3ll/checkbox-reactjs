import React, { useState, memo } from 'react';
import './App.css';
import { listCheckBox } from './data'
import CheckBox from './components/Checkbox'

// beurk
const CheckBoxListOld = memo(({ data }) => {
  const [selectedCheckbox, setSelectedCheckbox] = useState(data);
  const [allChecked, setAllChecked] = useState(false);
  const checkAllItems = (isChecked) => {
    setAllChecked(isChecked);
    setSelectedCheckbox(selectedCheckbox.map((item) => ({ ...item, isChecked })))
  };

  const setItemChecked = (id, isChecked) => {
    const items = selectedCheckbox.map((item) => {
      if (item.id === id) {
        return {
          ...item,
          isChecked
        }
      }
      return item;
    });

    setSelectedCheckbox(items);
    setAllChecked(items.every(item => item.isChecked));
  };

  return (
    <>
      <CheckBox title={!allChecked ? "Check All" : "Uncheck All"} checked={allChecked} onChange={() => checkAllItems(!allChecked)} />
      {selectedCheckbox.map(item => <CheckBox key={item.id} title={item.title} checked={item.isChecked} onChange={() => setItemChecked(item.id, !item.isChecked)} />)}
    </>
  );
});


const CheckBoxList = memo(({ data }) => {
  const [checkedItems, setCheckedItems] = useState(new Set([]));
  const checkAllItems = (isChecked) => setCheckedItems(new Set(isChecked ? data.map(item => item.id) : []));
  const setItemChecked = (id) => setCheckedItems(new Set([...Array.from(checkedItems.entries()).map(x => x[0]), id]));
  const allChecked = checkedItems.size === data.length;
  return (
    <>
      <CheckBox title={!allChecked ? "Check All" : "Uncheck All"} checked={allChecked} onChange={() => checkAllItems(!allChecked)} />
      {data.map(item => <CheckBox key={item.id} title={item.title} checked={checkedItems.has(item.id)} onChange={() => setItemChecked(item.id)} />)}
    </>
  );
});



export default () => {
  return <div className="App">
    <h4 className="title">React-hooks-multi-select-checkbox</h4>
    <CheckBoxList data={listCheckBox} />
  </div>
}
